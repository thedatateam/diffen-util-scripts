# Created by Dinesh Reddy (dinesh.reddy@thedatateam.in)
from datetime import datetime
import time
import requests
import json
from requests.auth import HTTPBasicAuth
import pandas as pd
import json
class API:    
    base_url = "http://10.12.20.4:18630" #Streamsets URI
    auth = HTTPBasicAuth('admin','admin') #username,password
    def get_request(self,path=''):
        return requests.get(self.base_url+path,auth=self.auth).json()
    def post_request(self,path='',data={}):
        headers= {'X-Requested-By':'sdc',
          'content-type':'application/json'
         }
        print(self.base_url+path)
        print(data)
        return requests.post(self.base_url+path,data = json.dumps(data),headers = headers,auth=self.auth).json()
def get_pipeline_metrics(api,pipeline_id):
    return api.get_request('/rest/v1/pipeline/'+pipeline_id+'/metrics?rev=0')

#pipeline id
pipeline_id = 'Finnone10952160PRODTestingnoprocessing957c8041-47d8-401f-b123-f52898adc4e0'

#frequency in seconds. 10*60 means every 10 minutes delay information will be recorded. 
frequency=10*60 

api=API()
#{field_name:field_key_in_API_response}
required_fileds={
    "read_lag":"['gauges']['custom.OracleCDCClient_01.Read Lag (seconds).0.gauge']['value']['delay']",
    "timeOfLastReceivedRecord":"['gauges']['RuntimeStatsGauge.gauge']['value']['timeOfLastReceivedRecord']",
    "Total rows IN":"['counters']['pipeline.batchInputRecords.counter']['count']",
    "Total rows OUT":"['counters']['pipeline.batchOutputRecords.counter']['count']",
    "CDC IN":"['counters']['pipeline.batchOutputRecords.counter']['count']",
    "CDC OUT":"['counters']['pipeline.batchOutputRecords.counter']['count']",
    "FileWriter IN":"['counters']['pipeline.batchOutputRecords.counter']['count']",
    "FWriter OUT":"['counters']['pipeline.batchOutputRecords.counter']['count']",    
    "jvm.memory.heap.used":"['gauges']['jvm.memory.heap.used']['value']",
    "jvm.memory.non-heap.used":"['gauges']['jvm.memory.non-heap.used']['value']",
    "jvm.memory.total.used":"['gauges']['jvm.memory.total.used']['value']",
    "jvm.threads.count":"['gauges']['jvm.threads.count']['value']",
    "jvm.threads.blocked.count":"['gauges']['jvm.threads.blocked.count']['value']",
    "jvm.threads.timed_waiting.count":"['gauges']['jvm.threads.timed_waiting.count']['value']",
    "jvm.threads.waiting.count":"['gauges']['jvm.threads.waiting.count']['value']",
    "jvm.threads.deadlock.count":"['gauges']['jvm.threads.deadlock.count']['value']"   
}

cols = ['date','time']
for i in list(required_fileds.keys()):
    cols.append(i)
csv_df = pd.DataFrame(columns=cols)
while(True):
    try:
        pipeline_mertics_json = get_pipeline_metrics(api,pipeline_id)
        csv_out_row = []
        date_now = datetime.now().strftime("%d-%m-%Y")
        time_now = datetime.now().strftime("%H:%M:%S")
        csv_out_row.append(date_now)
        csv_out_row.append(time_now)
        for field_key in required_fileds.values():
            value = eval('pipeline_mertics_json'+field_key)
            if(field_key=='timeOfLastReceivedRecord' ):
                csv_out_row.append(datetime.fromtimestamp(value/1000.).strftime('%c'))
            else:
                csv_out_row.append(value)
        csv_df.loc[len(csv_df)]=csv_out_row

        csv_df.to_csv('output.csv')
        with open('response_'+datetime.now().strftime("%d%m%Y_%H%M%S")+'.json','w') as f:
            json.dump(pipeline_mertics_json,f)
        print("iteration",datetime.now().strftime("%c"),"done...")
        time.sleep(frequency)        
    except:
        print("iteration",datetime.now().strftime("%c"),"failed")


