import cx_Oracle
import pandas as pd
import datetime,time
conn = cx_Oracle.connect("user_name", "password", "ip:port/service_name")

init_STARTTIME_STR="2020-11-12 00:00:00"
STARTTIME_OBJ=datetime.datetime.strptime(initial_STARTTIME_STR, '%Y-%m-%d %H:%M:%S')
ENDTIME_OBJ=STARTTIME_OBJ+datetime.timedelta(seconds=window)
window=300 #in seconds

while(True):
    exec_start_time=time.time()
    print(str(datetime.datetime.now()),": executing window:",str(STARTTIME_OBJ),"to",str(ENDTIME_OBJ))
    
    #do work
    sql_Logmnr_build="BEGIN DBMS_LOGMNR.START_LOGMNR( STARTTIME =>TO_DATE('"+str(STARTTIME_OBJ)+"','YYYY-MM-DD HH24:MI:SS'),ENDTIME => TO_DATE('"+str(ENDTIME_OBJ)+"','YYYY-MM-DD HH24:MI:SS'), OPTIONS => DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG + DBMS_LOGMNR.CONTINUOUS_MINE+DBMS_LOGMNR.COMMITTED_DATA_ONLY );END;"
    conn.cursor().execute(sql_Logmnr_build)
    
    sql="select * from \"V$LOGMNR_CONTENTS\" where table_name='NBFC_TXN_ADVICE_DTL'"
    df_ora = pd.read_sql(sql, con=conn)
    df_ora[["SEG_OWNER","TABLE_NAME","SQL_REDO"]].to_csv(str(STARTTIME_OBJ.strftime('D%Y%m%dT%H%M%S'))+".csv")
    
    #work done
    STARTTIME_OBJ=STARTTIME_OBJ+datetime.timedelta(seconds=window)
    ENDTIME_OBJ=ENDTIME_OBJ+datetime.timedelta(seconds=window)
    exec_end_time=time.time()
    print(str(datetime.datetime.now()),": finished window:",str(STARTTIME_OBJ),"to",str(ENDTIME_OBJ),"time_taken(seconds):",exec_end_time-exec_start_time)